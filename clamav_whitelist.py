#!/usr/bin/env python

import os
import datetime
import hashlib
import argparse

CLAMAVDIR = '/var/lib/clamav'
CLAMAVDB_MAIN = 'main'
CLAMAVDB_DAILY = 'daily'
CLAMAVDB_BYTECODE = 'bytecode'
CLAMAVDBS = [
    CLAMAVDB_MAIN,
    CLAMAVDB_DAILY,
    CLAMAVDB_BYTECODE
]
CLAMAV_EXT_CLD = 'cld'
CLAMAV_EXT_CVD = 'cvd'
CLAMAV_EXTS = [
    CLAMAV_EXT_CLD,
    CLAMAV_EXT_CVD
]
CLAMAV_WHITELIST_FILE_NAME = 'local.fp'
CLAMAV_WHITELIST_FILE_PATH = os.path.join(CLAMAVDIR, CLAMAV_WHITELIST_FILE_NAME)
ENTRY_FIELD_SUM = 'sum'
ENTRY_FIELD_SIZE = 'size'
ENTRY_FIELD_COMMENT = 'comment'

def clamavdir_is_valid():
    """
    Checks if the directory specified in CLAMAVDIR includes all the DB files
    typical for a ClamAV installation.
    """

    def clamav_db_file_exists(filename):
        """
        Checks if a file with the name given exists in the directory
        specified in the CLAMAVDIR constant.
        """
        filepath = os.path.join(CLAMAVDIR, filename)
        exists = os.path.exists(filepath) and os.path.isfile(filepath)
        return exists

    def clamav_db_exists(dbname):
        """
        Checks if at least one file with the given base
        and with one of the cld or cvd extensions exists.
        """
        pattern = '%(db)s.%(ext)s'
        [cld_filename, cvd_filename] = [
            pattern%{
                'db': dbname,
                'ext': ext
            }
            for ext in CLAMAV_EXTS
        ]
        cld_exists = clamav_db_file_exists(cld_filename)
        cvd_exists = clamav_db_file_exists(cvd_filename)
        exists = cld_exists or cvd_exists
        return exists

    success = True
    for db in CLAMAVDBS:
        db_exists = clamav_db_exists(db)
        success = success and db_exists
        if not success:
            break
    return success

def md5sum(filepath):
    """
    Computes the MD5 sum of a file given 
    and returns its sedecimal representation.
    Bottleneck: The file must fit in the available RAM
    (its contents get stored into a string variable).
    """
    file = open(filepath, 'rb')
    filebytes = file.read()
    hash = hashlib.md5(filebytes)
    result = hash.hexdigest()
    return result

def get_whitelist_comment(filepath):
    """
    Builds up a whitelist comment
    """
    today = datetime.date.today()
    id = today.strftime('%y%m%d')
    basename = os.path.basename(filepath)
    barename = os.path.splitext(basename)[0]
    result = '%s_%s'%(id, barename)
    return result

def get_whitelist_entry(filepath):
    """
    Constructs the whole whitelist entry,
    a line that should go into the local.fp file.
    """
    fields = {
        ENTRY_FIELD_SUM:        md5sum(filepath),
        ENTRY_FIELD_SIZE:       os.path.getsize(filepath),
        ENTRY_FIELD_COMMENT:    get_whitelist_comment(filepath)
    }
    return fields

def format_whitelist_entry(entry):
    result = '%(sum)s:%(size)s:%(comment)s'%entry
    return result

def get_whitelist_line(filepath):
    """
    Gets the line that should be inserted into the ClamAV whitelist file.
    """
    entry = get_whitelist_entry(filepath)
    line = format_whitelist_entry(entry)
    return line

class ClamAVWhitelist(dict):
    def __init__(self):
        """
        Manipulates the ClamAV local whitelist database.
        """
        self.parse()

    def parse(self):
        # Clear the whitelist contents before reading
        self.clear()
        if os.path.exists(CLAMAV_WHITELIST_FILE_PATH):
            # Open the ClamAV whitelist file
            whitelist_file = open(CLAMAV_WHITELIST_FILE_PATH, 'r')
            # Get all non-empty lines stripped from line endings and whitespace
            lines = (l.strip() for l in whitelist_file.readlines() if l.strip())
            # Parse all the file entries, one entry per line
            for line in lines:
                entry = self.parse_line(line)
                sum = entry[ENTRY_FIELD_SUM]
                size = entry[ENTRY_FIELD_SIZE]
                comment = entry[ENTRY_FIELD_COMMENT]
                self[(sum, size)] = comment


    def parse_line(self, line):
        parts = line.split(':')
        entry = {
            ENTRY_FIELD_SUM:        parts[0],
            ENTRY_FIELD_SIZE:       int(parts[1]),
            ENTRY_FIELD_COMMENT:    parts[2]
        }
        return entry

    def add_file(self, filepath):
        """
        Adds a file to the whitelist
        """
        sum = md5sum(filepath)
        size = os.path.getsize(filepath)
        comment = get_whitelist_comment(filepath)
        self[(sum, size)] = comment
    
    def save(self):
        """
        Saves the whitelist to the ClamAV whitelist file
        """
        whitelist_file = open(CLAMAV_WHITELIST_FILE_PATH, 'w')
        lines = (
            format_whitelist_entry(self.get_entry(sum, size)) + os.linesep
            for sum, size in sorted(self.keys())
        )
        whitelist_file.writelines(lines)

    def get_entry(self, sum, size):
        """
        Returns the whole whitelist entry as a dictionary
        """
        comment = self.get((sum, size))
        if comment is not None:
            result = {
                ENTRY_FIELD_SUM: sum,
                ENTRY_FIELD_SIZE: size,
                ENTRY_FIELD_COMMENT: comment
            }
        else:
            result = None
        return result

def whitelist(filepath):
    """
    Adds a whitelist entry for a file specified into the ClamAV system database
    """
    # Create the whitelist object
    wlobject = ClamAVWhitelist()
    # Add the file to the whitelist
    wlobject.add_file(filepath)
    # Save the whitelist
    wlobject.save()



if __name__ == '__main__':
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('filepath')
    ARGS = PARSER.parse_args()
    FILEPATH = ARGS.filepath
    # Check if the ClamAV directory specified in the CLAMAVDIR constant
    # contains the right files
    installation_is_ok = clamavdir_is_valid()
    
    if installation_is_ok:
        whitelist(FILEPATH)
    else:
        print("It seems that your ClamAV installation is corrupt somehow. "
        "Didn’t find all of its crucial databases in the “%s” directory "
        "(see the CLAMAVDIR constant in this script’s header)"%CLAMAVDIR)
        exit(1)
