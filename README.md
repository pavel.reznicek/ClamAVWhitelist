# ClamAV Whitelist

A simple tool written in Python 3 to add a file to the ClamAV whitelist 
so it doesn’t identify it as infected any more.

Useful if you are sure a file doesn’t contain a virus and want to stop ClamAV 
complaining about it. 

Still, the use of the `clamsubmit` utility is highly recommended 
(it doesn’t work for me, and hence this tool). 🙂

Distributed under the terms of the GNU General Public License, version 3. 
See the LICENSE file.